json.extract! car, :id, :marca, :modelo, :ano, :cor, :created_at, :updated_at
json.url car_url(car, format: :json)